#include <WiFi.h>
#include "time.h"
#include <ESP32Servo.h>
#include<EasyDDNS.h>

//servo object and servo pin
Servo myservo;
int servoPin = 27; 
int pos = 180;
int trigPin = 34;    // Trigger
int echoPin = 35;    // Echo
long duration;
float distancePre,distancePost;

//WiFi info
const char* ssid       = "Accesso Negato";//"Accesso Negato";
const char* password   = "fancylake958";//"fancylake958"; 
WiFiServer server(8000);

//Time struct and server
const char* ntpServer = "1.it.pool.ntp.org";
const char* ntpServer2 = "0.it.pool.ntp.org";
const long  gmtOffset_sec = 3600;
const int   daylightOffset_sec = 3600;
struct tm timeinfo;


// Variable to store the HTTP request and GET value
String header;
String aperturaStatus = "off";
char messaggio[50];

//Flags
String buttonState = "off";
bool tempFlag = false;
bool gotTime = false;


#define LED_BUILTIN 1

void wifiSettings(){
  IPAddress local_IP(192, 168, 1, 125);
  IPAddress gateway(192, 168, 1, 254);
  IPAddress subnet(255, 255, 255, 0);
  IPAddress primaryDNS(8, 8, 8, 8); //optional
  IPAddress secondaryDNS(8, 8, 4, 4);
  if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
    Serial.println("STA Failed to configure");
  }  
}


void servoOpen(){
  myservo.attach(servoPin);
  for (pos = 0; pos < 180; pos += 5) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
  delay(2000);
  
}

void servoClose(){
  myservo.attach(servoPin);
  for (pos = 180; pos > 0; pos -= 5) { // goes from 180 degrees to 0 degrees
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
  delay(1000);
  myservo.detach();
}

float getDistance(){
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  float cm=0;
  float distances[21];
  distances[0]=0;
  int t;
  for(int i=1;i<20;i++){
    digitalWrite(trigPin, LOW);
    delayMicroseconds(5);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
   
    // Read the signal from the sensor: a HIGH pulse whose
    // duration is the time (in microseconds) from the sending
    // of the ping to the reception of its echo off of an object.
    pinMode(echoPin, INPUT);
    duration = pulseIn(echoPin, HIGH);
   
    // Convert the time into a distance
    distances[i] = (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343 
    if (distances[i]>1000){
      distances[i]=distances[i-1];
    }
    cm = distances[i] + cm;
    Serial.println(distances[i]);
  }
  cm = cm/20;
  Serial.print("media: ");
  Serial.print(cm);
  return cm;
}



String checkDistance(float primaMisura, float secondaMisura){
  if (((primaMisura + 0.20) <= secondaMisura) && (secondaMisura <= 10.70)){
    return "<h1 style=\"color:MediumSeaGreen;\">V</h1>";
  }
  else {
    return "<h1 style=\"color:Tomato;\">X</h1>";
  }
}


/*
tm_sec  int seconds after the minute  0-60*
tm_min  int minutes after the hour  0-59
tm_hour int hours since midnight  0-23
tm_mday int day of the month  1-31
tm_mon  int months since January  0-11
tm_year int years since 1900  
tm_wday int days since Sunday 0-6
tm_yday int days since January 1  0-365
tm_isdst  int Daylight Saving Time flag */


void printLocalTime()
{
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return;
  }
  if ((timeinfo.tm_min%2 == 0)&&(tempFlag==false)){

    Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
    Serial.println("Opening Door by time clausole");
    aperturaStatus = "Open";
    tempFlag=true;
    strftime(messaggio,50, "%A, %B %d %Y %H:%M:%S",&timeinfo);
    distancePre = getDistance();
    Serial.println(distancePre);
    servoOpen();
    servoClose();
    delay(5000);
    distancePost=getDistance();
    Serial.println(distancePost);

  }else if((timeinfo.tm_min%2 == 1)&&(tempFlag!=false)){
    tempFlag = false;
    Serial.println("Resetting time clausole");
    aperturaStatus = "Closed";
    //servoClose();
  }else{
    if (tempFlag!=false){
    //Serial.println("Eccezione");
    }
  }
}

void printLocalTimeSetup()
{
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return;
  }
  Serial.println("Inizio tracking del tempo:");
  Serial.println(&timeinfo);
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
  gotTime=true;
}

void setup()
{
  Serial.begin(115200);

  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  
  myservo.attach(servoPin);
  tempFlag = false; 
  //connect to WiFi
 Serial.printf("Connecting to %s ", ssid);
 wifiSettings();
 WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
      delay(250);
      delay(250);
      Serial.print(".");
  }
  Serial.println(" CONNECTED");
  
  //init and get the time, stay in loop until it works with wifi
  int tentativoTempo=0;
  while(!getLocalTime(&timeinfo)){
    configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
    printLocalTimeSetup();
    tentativoTempo++;
  }
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  //disconnect WiFi as it's no longer needed, comment this if a web server is needed
  //WiFi.disconnect(true);
  //WiFi.mode(WIFI_OFF);
  server.begin();
}

void loop()
{
  delay(500);
  //EasyDDNS.update(10000);
  //struct tm timeinfo;
  printLocalTime();
  WiFiClient client = server.available();
   if (client) {                             // If a new client connects,
    Serial.println("New Client.");          // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
            // turns the GPIOs on and off
            if ((header.indexOf("GET /on") >= 0)&&(aperturaStatus=="Closed")) {
              Serial.println("Opening Door from web");
              aperturaStatus = "Open";
             //servoOpen();
            } else if ((header.indexOf("GET /off") >= 0)&&(aperturaStatus=="Open")) {
              Serial.println("Closing Door from web");
              aperturaStatus = "Closed";
              //servoClose();
            }
            //TODO display time of manual opening, go back to main page after command
            // Display the HTML web page
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, text/html;charset=utf-8\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            // CSS to style the on/off buttons 
            // Feel free to change the background-color and font-size attributes to fit your preferences
            client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
            client.println(".button { background-color: #4CAF50; border: none; color: white; padding: 16px 40px;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".button2 {background-color: #555555;}</style></head>");
            
            // Web Page Heading
            client.println("<body><h1>ESP32 Web Server</h1>");
           // for(int i=1; i<10;i++){
           //  client.println(char(176));
           //   delay(100);
           //    }
            client.println("<h2>Last opened: " + String(messaggio) + "  " + checkDistance(distancePre,distancePost) + "</h2>");
            // Display current state, and ON/OFF buttons for GPIO 26  
            client.println("<p>Door - Status: " + aperturaStatus + "</p>");
            // If the output26State is off, it displays the ON button       
            if (aperturaStatus=="Closed") {
              client.println("<p><a href=\"/on\"><button class=\"button\">Open</button></a></p>");
            } else {
              client.println("<p><a href=\"/off\"><button class=\"button button2\">Close</button></a></p>");
            } 
               
            client.println("</body></html>");
            
            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}